﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BotGameProbnick
{
    interface IGame
    {
        ConcurrentQueue<string> Rstream { set; get; } // readqueue
        void GameThread();
        void EndAllThreads();
        void PermitWorking();

    }
}
