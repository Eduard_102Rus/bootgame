﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections.Concurrent;
using System.IO;
using System.Threading;

namespace BotGameProbnick
{
    public class BrainInfo
    {
        public long? id;
        public string gameid;
        public long? playingwith;
        public bool first;
        public int state;
        public int question;
        public int category;
        public int score;
        public string answers;
    }

    class BrainComp : IGame
    {
        bool play;



        public ConcurrentQueue<string> Rstream { set; get; }

        private StringBuilder output;

        private string Input()
        {
            string result;
            Rstream.TryDequeue(out result);
            if (result != null)
                return result;
            else return null;
        }

        private void Output(ref StringBuilder output, long? ID)
        {
            Console.WriteLine(output);
            //Program.Consumer(output.ToString(), ID);
            output.Clear();
        }



        private string path;
        private Dictionary<int, string> Questions;

        private Dictionary<int, String> Categories;



        public static BrainInfo GetInfo(long? ID)
        {
            //DataBase[Convert.ToInt32(ID)].id = ID;
            //return DataBase[Convert.ToInt32(ID)];

            //return GameInfo.GetInfo(ID, num);

            return new BrainInfo();
        }

        public static void WriteToDB(BrainInfo Info)
        {
            //DataBase[Convert.ToInt32(Info.id)] = Info;
            //GameInfo.WriteToDB(Info);
        }

        public static void Delete(BrainInfo Info)
        {
            //DataBase.RemoveAt(Convert.ToInt32(Info.id));
            //GameInfo.Delete(Info.id, "!2");
        }


        private void Go(string input, ref BrainInfo Info)
        {
            BrainInfo SecondInfo;
            switch (Info.state)
            {
                case 0:
                    output.Append("Соперник найден");
                    Output(ref output, Info.id);
                    if (GetInfo(Info.playingwith).first)
                    {
                        Info.first = false;
                        output.Append("Очередь соперника отвечать");
                    }
                    else
                    {
                        Info.first = true;
                        output.Append("Ваша очередь отвечать\n\nВыберите категорию :\n");
                        for (int i = 0; i < Categories.Count; i++)
                        {
                            output.Append($"\n{i + 1}) {Categories[i]}");
                        }
                    }
                    Output(ref output, Info.id);
                    Info.state = 1;
                    break;
                case 1:
                    if (Info.first)
                    {
                        int result;
                        if (int.TryParse(Parse(input, ref Info), out result))
                        {
                            if (Categories.ContainsKey(result - 1))
                            {
                                Info.category = result;
                                Info.state = 2;
                                //Info.question =
                                return;
                            }
                            output.Append("Выберите из предложенного списка");
                            Output(ref output, Info.id);
                        }
                    }
                    else
                    {
                        output.Append("Пока что на вопросы отвечает соперник");
                    }
                    break;
                case 2:
                    Info.state = 3;
                    break;
                case 3:
                    SecondInfo = GetInfo(Info.playingwith);
                    if (SecondInfo.state == 3)
                    {
                        SecondInfo.state = 4;
                        WriteToDB(SecondInfo);
                        Info.state = 4;
                    }
                    break;
                case 4:
                    SecondInfo = GetInfo(Info.playingwith);
                    if (SecondInfo.score == Info.score)
                    {
                        output.Append("Ничья\n\n");
                        Output(ref output, Info.id);
                        output.Append("Ничья\n\n");
                        Output(ref output, SecondInfo.id);

                        output.Append("Хотите продолжить?\n\n1) Да\n2) Нет");
                        Output(ref output, Info.id);
                        output.Append("Хотите продолжить?\n\n1) Да\n2) Нет");
                        Output(ref output, SecondInfo.id);
                        Info.state = 5;
                        SecondInfo.state = 5;
                    }
                    else
                    {
                        if (SecondInfo.score > Info.score)
                        {
                            output.Append($"{SecondInfo.score} : {Info.score}\nПобеда!");
                            Output(ref output, SecondInfo.id);
                            output.Append($"{Info.score} : {SecondInfo.score}\nПоражение");
                            Output(ref output, Info.id);
                        }
                        else
                        {
                            output.Append($"{Info.score} : {SecondInfo.score}\nПобеда!");
                            Output(ref output, Info.id);
                            output.Append($"{SecondInfo.score} : {Info.score}\nПоражение");
                            Output(ref output, SecondInfo.id);
                        }
                    }
                    break;
                case 5:

                    break;
                case 6:
                    break;
            }
        }


        private string Parse(string input, ref BrainInfo Info)
        {
            return "0";
        }



        private int AskQuestion(BrainInfo Info, string input)
        {
            string[] text = File.ReadAllLines($"{Categories[Info.category]}/{Questions[Info.question]}");
            int correctanswer = 0;
            for (int i = 1; i < text.GetLength(0); i++)
            {
                if (text[i][0] == '*')
                {
                    correctanswer = i;
                }
            }

            output.Append($"{text[0]}\n\n");

            if (correctanswer != 0)
            {
                Random r = new Random();
                int counter = 1;
                while (counter < text.GetLength(0))
                {
                    int n = r.Next(text.GetLength(0) - 1);
                    if (text[n + 1] != "")
                    {
                        if (text[n + 1][0] == '*') correctanswer = counter;
                        output.Append($"{counter}) {text[n + 1]}\n");
                        text[n + 1] = "";
                        counter++;
                    }
                }
            }
            else
            {
                for (int i = 1; i < text.GetLength(0); i++) output.Append($"{i}) {text[i]}\n");
                correctanswer = 1;
            }

            Output(ref output, Info.id);

            return correctanswer;
        }



        public void PermitWorking()
        {
            play = true;
        }

        public void EndAllThreads()
        {
            play = false;
        }

        public void GameThread()
        {
            PermitWorking();
            while (true)
            {
                while (play)
                {

                }
                PermitWorking();
            }
        }

        public BrainComp()
        {

        }
    }
}
